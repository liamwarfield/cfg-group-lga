#!/usr/bin/env python3
from graphviz import Digraph
from functools import reduce
from collections import defaultdict
from prettytable import PrettyTable
from copy import deepcopy

class ParseNode:
    def __init__(self, parent, symbol, children=None):
        if children == None:
            children = []
        self.parent = parent
        self.children = children
        self.symbol = symbol
    
    def lable_in_order(self):
        self.id = 0
        id = 1
        for child in self.children:
            id = self.lable_in_order_rec(id, child)
    
    def lable_in_order_rec(self, id, parse_node):
        parse_node.id = id
        id += 1
        for child in parse_node.children:
            id = self.lable_in_order_rec(id, child)
        return id
    
    def visualize_rec(self, graph, parse_node):
        graph.node(repr(parse_node.id), parse_node.symbol)
        for child in parse_node.children:
            self.visualize_rec(graph, child)
            graph.edge(repr(parse_node.id), repr(child.id))

    def visualize(self, output_file='test-output/round-table.pdf'):
        graph = Digraph(comment='The Round Table')
        self.lable_in_order()
        graph.node(repr(self.id), self.symbol)
        for child in self.children:
            self.visualize_rec(graph, child)
            graph.edge(repr(self.id), repr(child.id))
        #graph.view()
        graph.render('test-output/round-table.gv', view=True)


class Item:
    # index represents where the marker is
    #   marker is to the left of "RHS[index]"
    def __init__(self, LHS, RHS, index):
        self.LHS = LHS
        self.RHS = RHS
        self.index = index

    def __eq__(self, other):
        return (
            self.__class__ == other.__class__ and
            self.LHS == other.LHS and
            self.RHS == other.RHS and
            self.index == other.index )
    
    def __repr__(self):
        return self.LHS + ' -> ' + str(self.RHS[:self.index] +  ['.'] + self.RHS[self.index:])


class Grammar:
    def __init__(self, filename):
        self.terminals = set([])
        self.nonterminals = []
        self.productions = {}
        self.num_to_RHS = []
        f = open(filename)
        self.item_set_dict = {}
        posterms = []
        RHS = ""
        LHS = ""
        for line in f:
            if "->" in line:
                (LHS, _, RHS) = line.split(maxsplit=2)
                if LHS not in self.nonterminals:
                    self.nonterminals.append(LHS)
            else:
                (_, RHS) = line.split(maxsplit=1)
            RHSs = RHS.split("|")
            for RHS in RHSs:
                if not LHS in self.productions:
                    self.productions[LHS] = [RHS.split()]
                    self.num_to_RHS.append(RHS.split())
                else:
                    self.productions[LHS].append(RHS.split())
                    self.num_to_RHS.append(RHS.split())
                if "$" in RHS:
                    self.start_sym = LHS
                posterms += RHS.split()
        
        for symbol in posterms:
            if not symbol in self.nonterminals:
                if symbol != "$" and symbol != "lambda":
                    self.terminals.add(symbol)
        


    # Populates an internal list with terminals that derive to lambda
    def create_lambda_table(self):
        self.lambda_table = []
        for nonterm in self.nonterminals:
            if self.derives_lambda(nonterm):
                self.lambda_table.append(nonterm)

    def derives_lambda(self, nonterminal, stack=[]):
        for RHS in self.productions[nonterminal]:
            if RHS == ["lambda"]:
                return True
            
            flag = False
            for symbol in RHS:
                if symbol in self.terminals or symbol == "$":
                    flag = True
            if flag:
                continue

            all_derive_lamdba = True
            for nonterminal in RHS:
                if (nonterminal, RHS) in stack:
                    continue
                stack.append((nonterminal, RHS))
                all_derive_lamdba = self.derives_lambda(nonterminal, stack=stack)
                stack.pop()
                if not all_derive_lamdba:
                    break
            if all_derive_lamdba:
                return True
        return False
    
    def create_first_table(self):
        self.first_table = {}
        for nonterminal in self.nonterminals:
            self.first_table[nonterminal] = self.first_set(nonterminal, [])

    def first_set(self, X, B, seen_syms=[]):
        if seen_syms == []:  # God damnmit PYTHON!!!
            seen_syms = set([])
        if X in self.terminals or X == "$" or X == "lambda":
            return set([X])
        F = set([])
        if X not in seen_syms:
            seen_syms.add(X)
            for RHS in self.productions[X]:
                temp = self.first_set(RHS[0], RHS[1:], seen_syms=seen_syms)
                F = F.union(temp)
        
        if X in self.lambda_table and B != []:
            if len(B) == 1:
                temp = self.first_set(B[0], [], seen_syms=seen_syms)
            else:
                temp = self.first_set(B[0], B[1:], seen_syms=seen_syms)
            F = F.union(temp)
        return F

    def create_follow_table(self):
        self.follow_table = {}
        for nontermial in self.nonterminals:
            self.follow_table[nontermial] = self.follow_set(nontermial)

    def follow_set(self, nonterm, seen=[]):
        if seen == []:
            seen = set([])
        if nonterm in seen:
            return set([])

        seen.add(nonterm)
        F = set([])

        for LHS in self.productions:
            for RHS in self.productions[LHS]:
                for position in range(len(RHS)):
                    if RHS[position] == nonterm:
                        if RHS[position + 1:] != []:  # if not at the end
                            F = F.union(self.first_set(RHS[position + 1], RHS[position + 2:]))

                        flag = True  # Check if everything can derive lambda
                        for symbol in RHS[position + 1:]:
                            if symbol in self.terminals or symbol == "$" or not self.derives_lambda(symbol):
                                flag = False
                                break
                        if RHS[position + 1:] == [] or flag:
                            F = F.union(self.follow_set(LHS, seen))
        return F

    def prediction_set(self, LHS, RHS):
        prediction = set([])
        prediction = prediction.union(self.first_set(RHS[0], RHS[1:]))

        if RHS == ["lambda"]: # Refactor target
            prediction = prediction.union(self.follow_set(LHS))
            return prediction

        flag = True
        for symbol in RHS:
            if symbol in self.terminals or symbol == "$" or not self.derives_lambda(symbol):
                flag = False
        if flag:
            prediction = prediction.union(self.follow_set(LHS))
        return prediction
    
    def create_ll1_table(self):
        self.ll1_table = {}
        for nonterm in self.nonterminals: # Create an empty table
            self.ll1_table[nonterm] = defaultdict(lambda: -1)

        rulenum = 0        
        for LHS in self.productions:
            for RHS in self.productions[LHS]:
                prediction = self.prediction_set(LHS, RHS)
                for terminal in prediction:
                    if self.ll1_table[LHS][terminal] != -1:
                        raise Exception("THIS IS NOT A LL1 CFG, Rule " + str(rulenum))
                    self.ll1_table[LHS][terminal] = rulenum
                rulenum += 1
    
    """
    Uses a LL1 table to parse some tokens!
    """
    def ll1_parse(self, tokenstream):
        tokenstream += "$"  # add end of input
        root_node = ParseNode(None, None)
        current_node = root_node
        stack = [self.start_sym]
        while len(stack) > 0:
            x = stack.pop()
            if x in self.nonterminals:
                prediction = self.ll1_table[x][tokenstream[0]]
                if prediction == -1:
                    raise Exception("Parsing ERROR")

                stack.append([])  # I'm using [] as a marker
                for symbol in reversed(self.num_to_RHS[prediction]):
                    stack.append(symbol)
                new_node = ParseNode(current_node, x)
                current_node.children.append(new_node)
                current_node = new_node
            elif x == []:
                current_node = current_node.parent
            elif x == "lambda":
                new_node = ParseNode(current_node, "λ")
                current_node.children.append(new_node)
            elif x in self.terminals or x == "$":
                if x != tokenstream[0]:
                    exp = tokenstream[0]
                    raise Exception(f"Parsing ERROR, expected '{exp}' got '{x}'")
                tokenstream.pop(0)
                new_node = ParseNode(current_node, x)
                current_node.children.append(new_node)
        return root_node

    def closure(self, itemset):
        itemcopy = itemset.copy()
        length = 0
        while len(itemcopy) != length:
            length = len(itemcopy)
            for item in itemcopy:
                if item.index >= len(item.RHS):
                    continue
                elif item.RHS[item.index] in self.productions:
                    for RHS in self.productions[item.RHS[item.index]]:
                        itemcheck = Item(item.RHS[item.index], RHS, 0)
                        if itemcheck not in itemcopy:
                            itemcopy.append(itemcheck)
        return itemcopy

    def goto(self, itemset, symbol):
        K = [deepcopy(item) for item in itemset if item.index != len(item.RHS) and item.RHS[item.index] == symbol]
        for item in K:
            item.index += 1
        return self.closure(K)
        '''
        # this would be the check for it the result of goto is in the grammar's item set dictionary
        K_closure = self.closure(K) # replace with result of goto
        for item_set_num in self.item_set_dict.keys():
            if item_set_dict[item_set_num][0] == K_closure[0]: # again, result of goto @ 0
                return item_set_num
        '''
            

    def __repr__(self):
        retstr = ""
        retstr += f"Terminals: {self.terminals}\n"
        retstr += f"Nonterminals: {self.nonterminals}\n"
        retstr += f"Grammar Rules\n"
        num = 0
        for LHS in self.productions:
            for rule in self.productions[LHS]:
                retstr += f"{num}. {LHS} -> {rule}\n"
                num += 1
        retstr += f"Start Symbol: {self.start_sym}\n"
        '''
        retstr += "LL1 table:\n"
        table = PrettyTable([""] + sorted(self.terminals))
        for nonterm in self.nonterminals:
            row = [nonterm]
            for terminal in sorted(self.terminals):
                rule = self.ll1_table[nonterm][terminal]
                if rule == -1:
                    row.append(" ")
                else:
                    row.append(rule)
            table.add_row(row)
        retstr += str(table)
        '''
        return retstr

grammar = Grammar('biglanguage.cfg')
print(grammar)
item_set_0 = [Item('S', ['T','U','C','$'], 0)]
item_set_0_closure = grammar.closure(item_set_0)
item_set_0_goto_x = grammar.goto(item_set_0_closure, 'x')
item_set_0_goto_t = grammar.goto(item_set_0_closure, 't')
item_set_0_goto_lambda = grammar.goto(item_set_0_closure, 'lambda')
item_set_0_goto_m = grammar.goto(item_set_0_closure, 'm')
item_set_0_goto_T = grammar.goto(item_set_0_closure, 'T')
item_set_0_goto_U = grammar.goto(item_set_0_closure, 'U')
print('Initial Item Set 0 (starts with only first prod. rule)')
print(item_set_0)
print('Closure of Item Set 0')
print(item_set_0_closure)
print('Goto on Item Set 0 on symbol \'x\'')
print(item_set_0_goto_x)
print('Goto on Item Set 0 on symbol \'t\'')
print(item_set_0_goto_t)
print('Goto on Item Set 0 on symbol \'lambda\'')
print(item_set_0_goto_lambda)
print('Goto on Item Set 0 on symbol \'m\'')
print(item_set_0_goto_m)
print('Goto on Item Set 0 on symbol \'T\'')
print(item_set_0_goto_T)
print('Goto on Item Set 0 on symbol \'U\'')
print(item_set_0_goto_U)

'''
gramr = Grammar("test.cfg")
gramr.create_lambda_table()
gramr.create_first_table()
gramr.create_follow_table()
gramr.create_ll1_table()
#gramr2 = Grammar("test_recursive_lambda.cfg")
gramr2.create_lambda_table()
gramr2.create_first_table()
gramr2.create_follow_table()
gramr2.create_ll1_table()
print(gramr)
#print(gramr2)
print(gramr.derives_lambda("A"))
print(gramr.derives_lambda("T"))
#print(gramr2.derives_lambda("A"))
#print(gramr2.derives_lambda("B"))
print(sorted(gramr.first_set("A", [])))
#print(sorted(gramr2.first_set("B", [])))
print(sorted(gramr.follow_set("VAR")))

print(gramr.closure([Item('S', ['A','$'], 0)]))
print(gramr.goto(gramr.closure([Item('S', ['A','$'], 0)]), 'e'))

parse_tree = gramr.ll1_parse("e equal f".split())
parse_tree.visualize()
'''
